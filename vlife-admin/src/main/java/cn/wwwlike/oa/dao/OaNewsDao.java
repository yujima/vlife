package cn.wwwlike.oa.dao;

import cn.wwwlike.oa.entity.OaNews;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class OaNewsDao extends DslDao<OaNews> {
}
